$(document).ready(function() {

	//Btn scroll Top
	$(window).on('scroll', function() {
		if($(window).scrollTop() > 1500) {
			$(".btn-scrl").fadeIn();
		}else {
			$(".btn-scrl").fadeOut();
		}
	});

	$(window).on('scroll', function() {
		if($(window).scrollTop() > 400) {
			$(".btn-scrlSP").fadeIn();
		}else {
			$(".btn-scrlSP").fadeOut();
		}
	});
//Accordion
	var plusSrc = ('./assets/img/sp/line-2.png');
	var subSrc = ('./assets/img/sp/line.png');

	var acc = $(".c-accordion").hide();
	var accPanel = $(".c-acc--panel");
	var i;
	var srcImgPlus = $("img.btn-plus").attr("src",plusSrc);
	var srcImgSub = $("img.btn-sub").attr("src",subSrc);
		for(i = 0; i < accPanel.length; i++){
			accPanel.eq(i).on('click', function() {
				$(this).find(srcImgPlus).toggle();
				$(this).find(srcImgSub).toggle();
				$(this).next().slideToggle('fast');
			});
		}

		//Animate
		$(".fade").on('inview', function(event, isInView, visiblePartX, visiblePartY) {
				if(isInView) {
					$(this).stop().addClass('fade-in');
				}
				else {
					$(this).stop().removeClass('fade-in');
				}
		})

		$(".show-off").on('inview', function(event, isInView, visiblePartX, visiblePartY) {
				if(isInView) {
					$(this).stop().addClass('show-up');
				}
				else {
					$(this).stop().removeClass('show-up');
				}
		})

});
