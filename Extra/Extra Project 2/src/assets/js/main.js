$(document).ready(function() {
	var animateCover = function() {
		if($(".mainVisual").length){
			$(".mainVisual .containerVisual").each(function() {
				var parent = $(this);
				$(".visual").css({
					backgroundSize: 'cover',
					backgroundPosition: 'center center',
					backgroundRepeat: 'no-repeat'
				});
				var noOfVisual = parent.find(".visual").length - 1;
				parent.find(".visual").eq(noOfVisual).animate({opacity: 0}, 1200, function() {
					parent.find(".visual").eq(noOfVisual).prependTo(parent).animate({opacity: 1});
				})
			});
		}
	}
	if($(".mainVisual").length){
		setInterval(function() {
			animateCover();
		}, 3000);
	}
	//BTN
	$(".c-btn_contact-4 a").hover(function(){
		$(this).find(".c-title1 img").attr('src', 'assets/img/title-5-hover.png');
	},function() {
		$(this).find(".c-title1 img").attr('src', 'assets/img/title-5.png');
	});
	$(".ex-ctn5").hover(function() {
		$(this).find(".c-title1 img").attr('src', 'assets/img/title-6-hover.png');
	},function() {
		$(this).find(".c-title1 img").attr('src', 'assets/img/title-6.png');
	});
	//Navbar
	$(".globalNav_items ul span").css({
		width: $('.current').outerWidth(),
		left: $('.current').position().left
	});
	$(".globalNav_items a").mouseover(function() {
		$(".globalNav_items span").stop().animate({
			width: $(this).outerWidth(),
			left: $(this).position().left
		}, 'fast');
	});
	//_slider
	$('.flexslider').flexslider({
    animation: "slide",
		direction: "horizontal",
		slideshow: true,
		animationSpeed: 600,
		thumbCaptions: true,
		directionNav: false,
		controlNav: false,
		slideshowSpeed: 3000,
  });
});
