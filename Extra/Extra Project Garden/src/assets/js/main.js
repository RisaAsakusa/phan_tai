$(document).ready(function() {
  $(".globalHeader_banner h2").hover(function() {
  	$(this).find("img").attr('src', './assets/img/catch-2-hover.png');
  }, function() {
  	$(this).find("img").attr('src', './assets/img/catch-2.png');
  });
  $(".globalFooter_goTop").hide();
  $(window).on('scroll', function() {
  	if($(window).scrollTop() > 500){
  		$(".globalFooter_goTop").fadeIn('slow').hover(function() {
  			$(this).find("img").attr('src', './assets/img/btn-scrl-active.png');
  		}, function() {
  			$(this).find("img").attr('src', './assets/img/btn-scrl.png');
  		});
  	}
  	else{
  		$(".globalFooter_goTop").fadeOut();
  	}
  });
});
