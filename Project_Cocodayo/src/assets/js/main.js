$(document).ready(function() {
	//Topic Accordion
	var accordion = $(".accordion").hide();
	var panel = $("dl.c-acc-panel");
	$("dl.c-acc-panel").on('click', function() {
		$(this).find(".accordion").toggle();
		$(this).find("img.plus").toggle();
		$(this).find("img.sub").toggle();
		$(this).find("img.sp-plus").toggle();
		$(this).find("img.sp-sub").toggle();
	});
	var topicContent  = $(".c-topic__list--content").hide();
	//QA Accordion
	var qaContent = $(".c-qa__inner--a").hide();
	var qaPanel = $(".c-qa__inner--q");

	for(i=0; i < qaPanel.length; i++) {
		qaPanel.eq(i).on('click', function() {

			$(this).find(".plus").toggle();
			$(this).find(".sub").toggle();
			$(this).find(".sp-plus").toggle();
			$(this).find(".sp-sub").toggle();
			$(this).next().slideToggle(300);
		});
	}
	//Global Menu
	$(".c-headerSP__menuSP").on('click', function() {
		$(".c-globalmenu").fadeIn(500);
	});
	$(".c-globalmenu__open").click(function() {
		$(".c-globalmenu").fadeOut(500);
	});
	$(".c-globalmenu").click(function() {
		$(this).fadeOut(500);
	});
	$(".c-globalmenu ul").click(function() {
		$("c.-globalmenu").fadeOut(500);
	});
});