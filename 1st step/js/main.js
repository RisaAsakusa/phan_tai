$(document).ready(function() {

  //Logo transform scale
  $(window).on("scroll", function(){
    if($(window).scrollTop()){
      $(".logo img").css({
          'width': '98px',
          'transition': '0.8s',
      });
      $(".logo-sm img").css({
        'width': '45px',
        'transition': '0.8s',
      });
    }
    else{
      $(".logo img").css({
          'width': '196px',
          'transition': '0.8s',
      });
      $(".logo-sm img").css({
        'width': '90px',
        'transition': '0.8s',
      });
    }
  });

  //Menu toggle
  $(".menu-icon").click(function() {
      $("#topnav").animate({'height': 'toggle',});
  });


  //Menu-icon

  $(".menu-icon").click(function() {
    $("div.icon:nth-child(1)").toggle();
    $("div.icon:nth-child(2)").toggleClass("rotate-bar-1");
    $("div.icon:nth-child(3)").toggleClass("rotate-bar-2");
  });

  //Menu-Access Section

  var page_url = window.location.href;
  var page_id = page_url.substring(page_url.lastIndexOf("#") + 1);

  if(page_id == "access"){
    $("html, body").animate({
      scrollTop: $("#access").offset().top - 160
    }, 700);
     $("#access").addClass("fadein-acc");
  }
  $("[href='gallery.html#access']").click(function() {
    $("#access").addClass("fadein-acc");
     $("html, body").animate({
      scrollTop: $("#access").offset().top - 160
    }, 700);
  });

  //Scroll page

  $(window).scrollTop(function() {
    $("#access").addClass("fadein-acc");
  });
});
