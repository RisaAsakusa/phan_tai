<?php
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
	//Theme Image Post
	add_theme_support( 'post-thumbnails' );
	//Image Size
	add_image_size('blog-thumbnail',700, 350, true);
	//Image Size
	add_image_size('post-large',900, 600, true);
	//Image Size
	add_image_size('blog-small',250, 200, true);

	//Menu
	function register_my_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
	}
	add_action( 'init', 'register_my_menu' );

	//Widgets

		function wpbeginner_widgets_init() {
	    register_sidebar( array(
	        'name'          => __( 'Sidebar', 'sidebar-mini' ),
	        'id'            => 'sidebar-mini',
	        'description'   => __( 'WPBeginner', 'sidebar-mini' ),
	        'before_widget' => '<div id="%1$s" class="card my-4 %2$s">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5 class="card-header">',
	        'after_title'   => '</h5>',
	    ) );
	}
	add_action( 'widgets_init', 'wpbeginner_widgets_init' );
	//Page_pagination
	function wpbeginner_pagination() {
        global $wp_query;

        $pages = paginate_links( array(
                'format'        => '?paged=%#%',
                'current'       => max( 1, get_query_var('paged') ),
                'total'         => $wp_query->max_num_pages,
                'type'          => 'array',
                'prev_next'     => true,
                'prev_text'     => __('← Older'),
                'next_text'     => __('Newer →'),
            )
        );

        if( is_array( $pages ) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            $pagination = '<ul class="pagination justify-content-center mb-4">';
            foreach ( $pages as $page ) {
                $pagination .= "<li class='page-item'>$page</li>";
            }
            $pagination .= '</ul>';

            echo $pagination;

        }
    }

    	//Comment

    function wpbeginner_comment( $comment, $args, $depth ) {
        $GLOBALS['comment'] = $comment;
        ?>
        <?php if ( $comment->comment_approved == '1' ): ?>
        <li class="media mb-4">
            <?php echo '<img class="d-flex mr-3 rounded-circle" src="'.get_avatar_url($comment).'" style="width: 50px;">' ?>
            <div class="media-body">
                <?php echo  '<h5 class="mt-0 mb-0"><a rel="nofllow" href="'.get_comment_author_url().'">'.get_comment_author().'</a> - </h5>' ?>
                <p class="mt-1">
                    <?php comment_text() ?>
                </p>

                <div class="reply">
                    <?php comment_reply_link(array_merge( $args, array('reply_text' => 'Reply','depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </div>
        </li>
        <?php endif;
    }