$(document).ready(function() {
	//Slider Header
	var animateCover = function(){
		if($("#main_visual").length){
			$("#main_visual .slider_container").each(function() {
				var parent = $(this);
				var slide = parent.find(".slide").length - 1;
				parent.find(".slide").eq(slide).animate({opacity:0}, 1500, function() {
					parent.find(".slide").eq(slide).prependTo(parent).animate({opacity:1});
				});
			});
		}
	}
	if($("#main_visual").length) {
		setInterval(function() {
			animateCover();
		}, 5000);
	}
	//Fixed scroll btn
	$(".c-btn-scrl").hide();
	$(window).on('scroll', function() {
		if($(this).scrollTop() > 1000) {
			$(".c-btn-scrl").fadeIn(500);
		}
		else {
			$(".c-btn-scrl").fadeOut(500);
		}
	});
	//Menu-sp
	$(".sp-menuicon").on('click', function() {
		$(".gnavi-sp").fadeIn(800);
	});
	$(".icon-ex").on('click', function() {
		$(".gnavi-sp").fadeOut(800);
	});

	var logoPc = './assets/img/pc/logo-pc.png';
	var logoPcHover = './assets/img/pc/logo-pc-2.png';
	var logoSp = './assets/img/sp/sp-logo.png';
	var logoSpHover = './assets/img/sp/sp-logo-white.png';

	$("img.logo-pc").hover(function() {
		$(this).attr('src', logoPcHover);
	}, function() {
		$(this).attr('src', logoPc);
	})
	$("img.logo-sp").hover(function() {
		$(this).attr('src', logoSpHover);
	},function() {
		$(this).attr('src', logoSp);
	})

});