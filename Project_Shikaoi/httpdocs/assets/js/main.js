$(document).ready(function() {
	//Navbar underline
	$(".navbar span").css({
		width: $(".current").outerWidth(),
		left: $(".current").position().left
	});
	$(".navbar a").mouseover(function() {
		$(".navbar span").stop().animate({
			width: $(this).outerWidth(),
			left: $(this).position().left
		}, 'fast');
	});
	//MenuSp
	$(".c-menuSP").click(function() {
		$(".c-globalMenuSP").slideToggle('fast');
		$(".c-menuSP").toggleClass("is-open");
	});
	//Fixed
	$(window).on('scroll', function() {
		if($(window).scrollTop()){
			$(".c-globalHeaderSP").addClass("fixed");
		}
		else{
			$(".c-globalHeaderSP").removeClass("fixed");
		}
	});

	//Main Visual
	var index = 0;
	var visuals = $(".mainVisual__child");
	var dots = $(".dot");
	var i;
	setInterval(function() {
		for(i=0; i < visuals.length; i++) {
			visuals.eq(i).hide();
		}
		for(i=0; i < visuals.length; i++) {
			dots.eq(i).removeClass("active");
		}
		index++;
		if(index > visuals.length){
			index = 1;
		}
		visuals.eq(index - 1).show();
		dots.eq(index - 1).addClass("active");
	}, 2500);

	//Slider Compt
	 $('.flexslider').flexslider({
	    animation: "slide",
	    directionNav: false,
	    pauseOnAction: false,
	    pauseOnHover: false,
	    slideshowSpeed: 1500,
	    animationSpeed: 800,
	    mousewheel: false
	  });
	 //Horizon Tab
	 $(".c-tab1__control--2 li a").on('click', function(e) {
	 	 $(".c-tab1__control--2 li a").removeClass("current-ctr");
	 	var idTab = $(this).attr('href');
	 	$(this).addClass("current-ctr");
	 	$(".c-tab1__content"+ idTab).fadeIn('slow').addClass("activeTab").siblings(".c-tab1__content").hide().removeClass("activeTab");
	 	$("body, html").animate({scrollTop: 0}, 200);
	 	// e.preventDefault();
	 });

	 //Accordion SM table
	 $(".c-table1--sm ul li:nth-child(odd)").on('click', function() {
	 	$(this).next().slideToggle('fast');
	 	$(this).toggleClass("accControl");
	 });
});
