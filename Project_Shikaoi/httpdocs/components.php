<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Global Component</title>
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/flexslider.css" rel="stylesheet">
<script src="assets/js/common.js"></script>
<script>
	$("head").append(
		'<style> body{display:none}'
	);
	$(function() {
		$("body").fadeIn("slow");
	});
</script>
</head>
<body>
	<!--Header-->
	<div id="wrapper">
		<?php
			//==============Header================
			include 'components/header.php';
			//==============Header=================
		?>
		<?php
			//==============Main Slider================
			include 'components/mainVisual.php';
			//==============Main Slider=================
		?>
		<?php
			//==============Imgtext1================
			include 'components/c-imgtitle1.php';
			//==============Imgtext1=================
		?>
		<br>
		<div class="l-main">
			<?php
				//==============cTitle1===============
				include 'components/c-title1.php';
				//==============cTitle1=================
			?>
			<?php
				//==============cTitle1-Img===============
				include 'components/c-title1-img.php';
				//==============cTitle1-Img=================
			?>
			<?php
				//==============cTitle2===============
				include 'components/c-title2.php';
				//==============cTitle2=================
			?>
			<?php
				//==============cTitle2--Color2===============
				include 'components/c-title2-color2.php';
				//==============cTitle2--Color2=================
			?>
			<?php
				//==============cTitle3===============
				include 'components/c-title3.php';
				//==============cTitle3=================
			?>
			<?php
				//==============cImageText1===============
				include 'components/c-imgtext-1.php';
				//==============cImageText1=================
			?>
			<?php
				//==============cImageText2===============
				include 'components/c-imgtext-2.php';
				//==============cImageText2=================
			?>
			<?php
				//==============cImageText3===============
				include 'components/c-imgtext-3.php';
				//==============cImageText3=================
			?>
			<?php
				//==============cImageText3--Border===============
				include 'components/c-imgtext-3-border.php';
				//==============cImageText3--Border=================
			?>
			<?php
				//==============cImageText4===============
				include 'components/c-imgtext-4.php';
				//==============cImageText4=================
			?>
			<?php
				//==============cImageText5===============
				include 'components/c-imgtext-5.php';
				//==============cImageText5=================
			?>
			<?php
				//==============cImageText5===============
				include 'components/c-imgtext-5-white.php';
				//==============cImageText5=================
			?>
			<?php
				//==============cImageText6===============
				include 'components/c-imgtext-6.php';
				//==============cImageText6=================
			?>
			<br>
			<?php
				//==============cImageText6===============
				include 'components/c-imgtext-6-magazine.php';
				//==============cImageText6=================
			?>
			<br>
			<?php
				//==============cImageText6===============
				include 'components/c-imgtext-6-youth.php';
				//==============cImageText6=================
			?>
			<br>
			<?php
				//==============cImageText6===============
				include 'components/c-imgtext-6-senior.php';
				//==============cImageText6=================
			?>
			<br>
			<?php
				//==============cImageText3 Large===============
				include 'components/c-imgtext-3-lg.php';
				//==============cImageText3 Large=================
			?>
			<?php
				//==============c-ListSitemap===============
				include 'components/c-listsitemap.php';
				//==============c-ListSitemap=================
			?>
			<?php
				//==============c-ListInfo===============
				include 'components/c-listsinfo.php';
				//==============c-ListInfo=================
			?>
			<br>
			<?php
				//==============c-Table1===============
				include 'components/c-table-1.php';
				//==============c-Table1=================
			?>
			<br>
			<?php
				//==============c-Table2===============
				include 'components/c-table-2.php';
				//==============c-Table2=================
			?>
			<br>
			<?php
				//==============c-Table3===============
				include 'components/c-table-3.php';
				//==============c-Table3=================
			?>
			<br>
			<?php
				//==============c-Table4===============
				include 'components/c-table-4.php';
				//==============c-Table4=================
			?>
			<br>
			<?php
				//==============c-Text1===============
				include 'components/c-text-1.php';
				//==============c-Text1=================
			?>
			<br>
			<?php
				//==============c-Text2===============
				include 'components/c-text-2.php';
				//==============c-Text2=================
			?>
			<div style="width: 560px;margin-top: 10px">
				<?php
					//==============c-Btn===============
					include 'components/c-btn.php';
					//==============c-Btn=================
				?>
			</div>
			<?php
				//==============Slide===============
				include 'components/c-slide.php';
				//==============Slide=================
			?>
		</div>
		<?php
			//==============Footer=================
			include 'components/footer.php';
			//==============Footer=================
		?>
	</div>
	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/main.js"></script>
	<script src="assets/js/jquery.flexslider-min.js"></script>
</body
</html>