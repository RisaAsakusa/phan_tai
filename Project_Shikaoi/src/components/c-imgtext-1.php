<!--Image Text 1-->
	<div class="c-imgtext1">
		<div class="c-imgtext1__img">
			<a href="#">
				<img src="assets/img/pc/01-index/imgtext1-1.jpg" alt="">
			</a>
		</div>
		<div class="c-imgtext1__txt">
			<p>
				<span class="u-grey">Aコープ鹿追店</span>
				<img src="assets/img/pc/imgtext1-new.png" alt="">
			</p>
			<h3>国産野菜統一宣言！ 地産地消をおいしく応援中。</h3>
			<p>Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、</p>
			<p>安心で安全な食品をご提供いたします。</p>
		</div>
		<div class="c-imgtext1__btn">
			<a href="">
				<img src="assets/img/pc/icon-1.png" alt="">
			</a>
		</div>
	</div>
<!--Image Text 1-->