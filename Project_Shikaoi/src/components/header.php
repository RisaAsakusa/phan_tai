	<!--Header-->
		<div class="pc-only">
			<header class="c-globalHeaderPC">
				<div class="c-globalHeaderPC__inner">
					<h1>
						<a href="index.html">
							<img src="assets/img/logo.png" alt="">
						</a>
					</h1>
					<nav class="c-globalHeaderPC__gnavi">
						<ul class="navbar">
							<li>
								<a href="index.html" class="current">ホーム</a>
							</li>
							<li>
								<a href="outline.html">JA鹿追町について</a>
							</li>
							<li>
								<a href="shikaoi.html">鹿追町の農業</a>
							</li>
							<li>
								<a href="community.html">青年部・女性部・熟年会</a>
							</li>
							<li>
								<a href="workplace.html">職場紹介</a>
							</li>
							<li>
								<a href="member.html">組合員情報</a>
							</li>
							<li>
								<a href="recruit.html">求人情報</a>
							</li>
							<li>
								<a href="topics.html">新着情報</a>
							</li>
							<span></span>
						</ul>
					</nav>
				</div>
			</header>
		</div>
		<div class="sp-only">
			<header class="c-globalHeaderSP">
				<div class="c-globalHeaderSP__inner">
					<h2>
						<img src="assets/img/logo.png" alt="" width="110">
					</h2>
					<div class="c-menuSP">
						<div class="c-menuSP__1"></div>
						<div class="c-menuSP__2"></div>
						<div class="c-menuSP__3"></div>
					</div>
					<nav class="c-globalMenuSP">
						<ul>
							<li>
								<a href="index.html">ホーム</a>
							</li>
							<li>
								<a href="outline.html">JA鹿追町について</a>
							</li>
							<li>
								<a href="shikaoi.html">鹿追町の農業</a>
							</li>
							<li>
								<a href="community.html">青年部・女性部・熟年会</a>
							</li>
							<li>
								<a href="workplace.html">職場紹介</a>
							</li>
							<li>
								<a href="member.html">組合員情報</a>
							</li>
							<li>
								<a href="recruit.html">求人情報</a>
							</li>
							<li>
								<a href="topics.html">新着情報</a>
							</li>
						</ul>
					</nav>
			</header>
		</div>
		<!--Header-->