<!--Footer-->
		<footer class="c-globalFooterPC">
			<div class="c-globalFooterPC__inner">
				<div class="c-globalFooterPC__inner--left">
					<p>JA鹿追町　〒081-0341 北海道河東郡鹿追町新町4丁目51番地</p>
					<p>
						<span>Tel. 0156-66-2131</span>
						<span>Fax. 0156-66-3194</span>
					</p>
					<div class="c-btn1">
						<a href="access.html">アクセスマップ</a>
					</div>
					<div class="c-btn1">
						<a href="https://www.shikaoi-study.jp/contact/">お問い合わせ</a>
					</div>
				</div>
				<div class="c-globalFooterPC__inner--right">
					<ul>
						<li>
							<a href="privacy.html">個人情報保護方針の取り扱い</a>
						</li>
						<li>
							<a href="ja_bank.html">貯金苦情受付</a>
						</li>
						<li>
							<a href="ja_kyosai.html">共済苦情受付</a>
						</li>
						<li>
							<a href="sitemap.html">サイトマップ</a>
						</li>
						<li>
							<a href="links.html">リンク</a>
						</li>
					</ul>
				</div>
			</div>
		</footer>
		<!--Footer-->