<div class="mainVisual">
	<div class="mainVisual__contain">
		<div class="mainVisual__child fade">
			<img src="assets/img/pc/visual-1.jpg" alt="">
			<h2 class="catchCopy">
				<img src="assets/img/pc/catchCopy.png" alt="">
			</h2>
		</div>
		<div class="mainVisual__child fade">
			<img src="assets/img/pc/visual-1.jpg" alt="">
			<h2 class="catchCopy">
				<img src="assets/img/pc/catchCopy.png" alt="">
			</h2>
		</div>
		<div class="mainVisual__child fade">
			<img src="assets/img/pc/visual-1.jpg" alt="">
			<h2 class="catchCopy">
				<img src="assets/img/pc/catchCopy.png" alt="">
			</h2>
		</div>
		<div class="mainVisual__child fade">
			<img src="assets/img/pc/visual-1.jpg" alt="">
			<h2 class="catchCopy">
				<img src="assets/img/pc/catchCopy.png" alt="">
			</h2>
		</div>
		<div class="mainVisual__child fade">
			<img src="assets/img/pc/visual-1.jpg" alt="">
			<h2 class="catchCopy">
				<img src="assets/img/pc/catchCopy.png" alt="">
			</h2>
		</div>
		<div class="mainVisual__dots">
			<span class="dot"></span>
			<span class="dot"></span>
			<span class="dot"></span>
			<span class="dot"></span>
			<span class="dot"></span>
		</div>
	</div>
</div>