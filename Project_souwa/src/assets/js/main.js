$(document).ready(function() {

	//Background-Slide
	var animateCover = function(){
		if($("#main_slider").length){
			$("#main_slider .slider-container").each(function() {
				var parent = $(this);
				var nbOfVisus = parent.find(".slide").length - 1;
				parent.find(".slide").eq(nbOfVisus).animate({opacity:0}, 1500, function() {
					parent.find(".slide").eq(nbOfVisus).prependTo(parent).css({opacity:1});
				})
			});
		}
	}
	if($("#main_slider").length){
		setInterval(function() {
			animateCover();
		}, 4000);
	}

	//Lightbox

	$(".n_lbox-1").on('click', function() {
		$(".backdrop, .light-box").animate({'opacity':'1'}, 400, 'linear');
		$(".backdrop, .light-box").css({'display':'block'});
		$("#lb-1").css('display','block');
		$("#lb-2").css('display','none');
	});

	$(".n_lbox-2").on('click', function() {
		$(".backdrop, .light-box").animate({'opacity':'1'}, 400, 'linear');
		$(".backdrop, .light-box").css({'display':'block'});
		$("#lb-1").css('display','none');
		$("#lb-2").css('display','block');
	});

	$(".c_lbox-1").on('click', function() {
		$(".backdrop, .light-box").animate({'opacity': '1'}, 400, 'linear');
		$(".backdrop, .light-box").css({display:'block'});
		$("#lb-3").css('display','block');
		$("#lb-4").css('display','none');
		$("#lb-5").css('display','none');
	});

	$(".c_lbox-2").on('click', function() {
		$(".backdrop, .light-box").animate({'opacity': '1'}, 400, 'linear');
		$(".backdrop, .light-box").css({display:'block'});
		$("#lb-3").css('display','none');
		$("#lb-4").css('display','block');
		$("#lb-5").css('display','none');
	});

	$(".c_lbox-3").on('click', function() {
		$(".backdrop, .light-box").animate({'opacity': '1'}, 400, 'linear');
		$(".backdrop, .light-box").css({display:'block'});
		$("#lb-3").css('display','none');
		$("#lb-4").css('display','none');
		$("#lb-5").css('display','block');
	});

	$(".close").on('click', function() {
		close();
	});
	$(".backdrop").on('click', function() {
		close();
	});

	function close() {
		$(".backdrop, .light-box").animate({'opacity':'0'}, 400, function(){
				$(".backdrop, .light-box").css({'display':'none'});
		});
	}

	//Fade
	$(".fade-in").on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView) {
			$(this).stop().addClass("fade-out");
		}
		else {
			$(this).stop().removeClass("fade-out");
		}
	});

	//Scale
	$(".scale-off").on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView) {
			$(this).stop().addClass("scale-out");
		}
		else {
			$(this).stop().removeClass("scale-out");
		}
	});

	//Inview Images
	$(".inview-off").on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if(isInView){
			$(this).stop().addClass('inview');
		}
		else {
			$(this).stop().removeClass("inview");
		}
	});

	//Button Click;

	$("button.confirm").on('click', function() {
		$(this).hide();
		$("button.back").css('display', 'inline-block');
		$("button.send").css('display', 'inline-block');
	});
	$("button.back").on('click', function() {
		$(this).hide();
		$("button.send").hide();
		$("button.confirm").css('display', 'block');
	});

	$("button.send").on('click', function() {
		alert("Thank you !!!");
		window.location.replace("index.html");
	});
});