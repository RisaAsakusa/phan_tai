$(document).ready(function() {
	var srcImg = './assets/img/pc/header-top-2.png';
	var srcImghover = './assets/img/pc/header-top-2-hover.png';
	var srcImgFooter = './assets/img/pc/f-head-1.png';
	var srcImgHoverFooter = './assets/img/pc/f-head-1-hover.png';
	var srcImgBtn3 = './assets/img/pc/btn-3.png';
	var srcImgBtn3Hover = './assets/img/pc/btn-3-hover.png';

	$("a.c-btn--img").hover(function(){
		$(this).find("img").attr('src', srcImghover);
	}, function(){
		$(this).find("img").attr('src', srcImg);
	})

	$("a.c-btn--imgFooter").hover(function(){
		$(this).find("img").attr('src', srcImgHoverFooter);
	}, function(){
		$(this).find("img").attr('src', srcImgFooter);
	})
	$("a.c-btn3").hover(function(){
		$(this).find("img").attr('src', srcImgBtn3Hover);
	}, function(){
		$(this).find("img").attr('src', srcImgBtn3);
	})

	$(window).on('scroll', function() {
		if($(this).scrollTop() > 500){
			$(".btn-scrl").fadeIn('slow');
		}
		else{
			$(".btn-scrl").fadeOut('slow');
		}
	});

	$(".btn-scroll-bot").on('click', function() {
		$("html, body").animate({
			scrollTop: $(document).height()
		}, 800);
		return false;
	});

	$("#btn-submit").on('click', function() {
		alert("Thank you");
		$(html).location.href("index.html");
	});
});