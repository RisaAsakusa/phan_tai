$(document).ready(function() {
	//Toggle menu
	$(".menu-icon").on('click', function() {
		// $(".nav-bar-sp").animate({'height': 'toggle'}).css('transition','0.8s');
		$(".nav-bar-sp").toggle();

		$(".icon:first-child").toggle().css('transition','0.5s');
		$(".icon:nth-child(2)").toggleClass('rotate-bar-1');
		$(".icon:nth-child(3)").toggleClass('rotate-bar-2');
	});
});