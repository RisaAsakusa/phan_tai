$(document).ready(function() {

  $(window).on('scroll', function() {
    if($(this).scrollTop() > 500){
      $(".c-btn-fix").fadeIn('slow');
    }
    else{
        $(".c-btn-fix").fadeOut('slow');
    }
  });
  $(".c-menuSP").on('click', function() {
    $(".g-naviSP").fadeIn('fast');
  });
  $(".menu-icon").on('click', function() {
      $(".g-naviSP").fadeOut('fast');
  });
  $(".c-header__top--btn").on('click', function() {
    $(".modalSP").fadeIn();
  });
  $(".modalSP__close").on('click', function() {
    $(".modalSP").fadeOut('fast');
  });
  //Slider
  $(".slider").slick({
    autoplay: true,
    autoplaySpeed: 2000,
    prevArrow: '<button type="button" class="prev"><img src="assets/img/slide/pc/slide-prev.png"></button>',
    nextArrow: '<button type="button" class="next"><img src="assets/img/slide/pc/slide-arr-next.png"></button>',
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="prev"><img src="assets/img/slide/sp/prev.png" width="10"></button>',
        nextArrow: '<button type="button" class="next"><img src="assets/img/slide/sp/next.png" width="10"></button>',
      }
    }]
  });
  $(".slider2").slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false,
        infinite: true,
        prevArrow: '<button type="button" class="prev"><img src="assets/img/slide/sp/prev.png" width="10"></button>',
        nextArrow: '<button type="button" class="next"><img src="assets/img/slide/sp/next.png" width="10"></button>',
      }
    }]
  });

   $(".sec7-sp").slick({
    autoplay: true,
    autoplaySpeed: 4000,
    prevArrow: '<button type="button" class="prev"><img src="assets/img/slide/pc/slide-prev.png"></button>',
    nextArrow: '<button type="button" class="next"><img src="assets/img/slide/pc/slide-arr-next.png"></button>',
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="prev"><img src="assets/img/slide/sp/prev.png" width="10"></button>',
        nextArrow: '<button type="button" class="next"><img src="assets/img/slide/sp/next.png" width="10"></button>',
      }
    }]
  });

  //Accordion
  $(".accor-qa ul li:nth-child(odd)").on('click', function() {
    $(this).toggleClass("active");
    $(this).next().slideToggle('fast');
  });

  $("#btn-cont").on('click', function() {
    alert("Thank you");
    window.location.href("wrapper");
  });
});
