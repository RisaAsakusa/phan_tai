//Slider 1
var slideIndex = 1;
showSlides_1(slideIndex);

function plusSlides_1(n) {
  showSlides_1(slideIndex += n);
}

function currentSlide_1(n) {
  showSlides(slideIndex = n);
}

function showSlides_1(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");

  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

//Slider 2
var slideIndex = 1;
showSlides_2(slideIndex);

function plusSlides_2(n) {
  showSlides_2(slideIndex += n);
}

function currentSlide_2(n) {
  showSlides_2(slideIndex = n);
}

function showSlides_2(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides-2");
  var dots = document.getElementsByClassName("dot-2");

  console.log(slides);
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}


//Slider 3
var slideIndex = 1;
showSlides_3(slideIndex);

function plusSlides_3(n) {
  showSlides_3(slideIndex += n);
}

function currentSlide_3(n) {
  showSlides_3(slideIndex = n);
}

function showSlides_3(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides-3");
  var dots = document.getElementsByClassName("dot-3");

  console.log(slides);
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}